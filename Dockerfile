FROM node:8.11.1-alpine

RUN apk update
RUN apk add python python-dev
RUN apk add make gcc g++
RUN apk add openssl-dev
RUN apk add ca-certificates wget --update-cache
RUN wget https://yarnpkg.com/latest.tar.gz
RUN tar -xzf latest.tar.gz && mv yarn-* yarn && rm latest.tar.gz
ENV PATH $PATH:/opt/yarn/bin/
# Create app directory
RUN mkdir -p /app
RUN mkdir -p /files/images
WORKDIR /app

# Copy project file under app dir
COPY . /app

# Install Depedencies project
RUN yarn install

EXPOSE 8000
VOLUME /files/images
CMD [ "npm", "start" ]
