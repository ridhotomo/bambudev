module.exports = {
  setupTestFrameworkScriptFile: './jest.setup.js',
  testURL: 'http://localhost:3000',
  bail: true,
  testEnvironment: 'node',
};
