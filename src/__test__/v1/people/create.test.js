const mongoose = require('mongoose');
const config = require('../../../app/config');
const app = require('./../../../app/app');
const request = require('supertest');

const PeopleModel = require('./../../../app/v1/people/models');

function createConn() {
  mongoose.Promise = global.Promise;

  const uri = config.mongoUri || '';
  mongoose.connect(uri);
}

const cleanCollection = (async () => {
  await PeopleModel.People.remove({});
});

describe('Create Peoples', () => {
  beforeAll(async () => {
    await createConn();
  });

  afterAll(async () => {
    await setTimeout(() => process.exit(), 1000);
  });


  beforeEach(async () => {
    await cleanCollection();
  });

  test('It should create 201 people the POST method', async (done) => {
    const payload = {
      name: 'Eminem',
      age: 32,
      latitude: 40.74322,
      longitude: 19.55556,
      monthlyIncome: 70302,
      experienced: false,
      score: 0.3,
    };
    request(app)
      .post('/v1/people-like-you')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send(payload)
      .end((err, res) => {
        const {
          body,
        } = res;
        expect(res.statusCode).toBe(201);
        expect(body.status).toBeDefined();
        expect(body.data).toBeDefined();
        expect(body.data.name).toEqual('Eminem');
        expect(body.data.age).toEqual(32);
        expect(body.data.latitude).toEqual(40.74322);
        expect(body.data.longitude).toEqual(19.55556);
        expect(body.data.experienced).toEqual(false);
        expect(body.data.score).toEqual(0.3);
        done();
      });
  });

  test('It should not create people the POST method when not pass validation', async (done) => {
    const payload = {
      age: 32,
      latitude: 40.74322,
      longitude: 19.55556,
      monthlyIncome: 70302,
      experienced: false,
      score: 0.3,
    };
    request(app)
      .post('/v1/people-like-you')
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send(payload)
      .end((err, res) => {
        const {
          body,
        } = res;
        expect(res.statusCode).toBe(400);
        expect(body.status).toBeDefined();
        expect(body.data).toEqual({ name: ['Name can\'t be blank'] });
        done();
      });
  });
});
