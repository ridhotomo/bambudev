const mongoose = require('mongoose');
const config = require('../../../app/config');
const app = require('./../../../app/app');
const request = require('supertest');

const { People } = require('./../../../app/v1/people/models');

function createConn() {
  mongoose.Promise = global.Promise;

  const uri = config.mongoUri || '';
  mongoose.connect(uri);
}

const cleanCollection = (async () => {
  await People.remove({});
});

/**
 * Create sample data for test
 */
const samplePeople = (async () => {
  const payload = [{
    name: 'Logan',
    age: 37,
    latitude: 40.74222,
    longitude: 19.54556,
    monthlyIncome: 73302,
    experienced: false,
    score: 0.9,
  },
  {
    name: 'Wolfvrine',
    age: 40,
    latitude: 42.74222,
    longitude: 39.54556,
    monthlyIncome: 80000,
    experienced: true,
    score: 0.6,
  },
  ];
  return People.create(payload);
});

describe('List Peoples', () => {
  beforeAll(async () => {
    await createConn();
  });

  afterAll(async () => {
    await setTimeout(() => process.exit(), 1000);
  });


  beforeEach(async () => {
    await cleanCollection();
    await samplePeople();
  });

  test('It should response success 200 the GET method', async (done) => {
    request(app)
      .get('/v1/people-like-you?age=37')
      .end((err, res) => {
        const {
          body,
        } = res;
        expect(res.statusCode).toBe(200);
        expect(body.data).toBeDefined();
        expect(body.status).toEqual('success');
        expect(body.data.peopleLikeYou).toBeDefined();
        expect(body.data.peopleLikeYou[0].age).toEqual(37);
        done();
      });
  });

  test('It should response success 200 the GET method', async (done) => {
    request(app)
      .get('/v1/people-like-you?experienced=false')
      .end((err, res) => {
        const {
          body,
        } = res;
        expect(res.statusCode).toBe(200);
        expect(body.status).toBeDefined();
        expect(body.data).toBeDefined();
        expect(body.status).toEqual('success');
        expect(body.data.peopleLikeYou).toBeDefined();
        expect(body.data.peopleLikeYou[0].experienced).toEqual(false);
        done();
      });
  });

  test('It should response success 200 the GET method', async (done) => {
    request(app)
      .get('/v1/people-like-you?latitude=40.74222')
      .end((err, res) => {
        const {
          body,
        } = res;
        expect(res.statusCode).toBe(200);
        expect(body.status).toBeDefined();
        expect(body.data).toBeDefined();
        expect(body.status).toEqual('success');
        expect(body.data.peopleLikeYou).toBeDefined();
        expect(body.data.peopleLikeYou[0].latitude).toEqual(40.74222);
        done();
      });
  });

  test('It should response success 200 the GET method', async (done) => {
    request(app)
      .get('/v1/people-like-you?latitude=4022')
      .end((err, res) => {
        const {
          body,
        } = res;
        expect(res.statusCode).toBe(200);
        expect(body.status).toBeDefined();
        expect(body.data).toBeDefined();
        expect(body.status).toEqual('success');
        expect(body.data.peopleLikeYou.length).toEqual(0);
        done();
      });
  });

  test('It should response success 200 the GET method', async (done) => {
    request(app)
      .get('/v1/people-like-you?latitude=40.74222&experienced=false&age=37')
      .end((err, res) => {
        const {
          body,
        } = res;
        expect(res.statusCode).toBe(200);
        expect(body.status).toBeDefined();
        expect(body.data).toBeDefined();
        expect(body.status).toEqual('success');
        expect(body.data.peopleLikeYou).toBeDefined();
        done();
      });
  });
});
