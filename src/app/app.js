const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');

const http = require('http-status-codes');
const jsend = require('jsend');

const config = require('./config');
const utils = require('./utils');
const errors = require('./errors');
const mongo = require('./mongo');

const ping = require('./v1/ping');
const people = require('./v1/people');
// const users = require('./routes/users');
// const leaves = require('./routes/leaves');
// const upload = require('./routes/upload');

// // just for research
// const pushyme = require('./research');

const app = express();
const { logger } = utils;

/*
Install default middlewares
*/
app.use(helmet());
app.use(morgan('common'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(jsend.middleware);
app.use(cors());

/*
Install apps setup versioning router
to future development when improve another versions
ex : add const v2 = express.Router()
    app.user('/v2', v2)
    v2.use('/user', user) # localhost:3000/v2/user
*/

const v1 = express.Router();
app.use('/v1', v1);

v1.use('/ping', ping);
v1.use('/people-like-you', people);

/*
Should be used as final error handler
*/
app.use((err, req, res, next) => { // eslint-disable-line
  // for now we only need to log it
  logger.error(err);

  if (err instanceof errors.AppError) {
    res.status(err.status).jsend.error(err.message);
  } else if (err.name === 'UnauthorizedError') {
    res.status(http.UNAUTHORIZED).jsend.fail(err.message);
  } else {
    res.status(http.INTERNAL_SERVER_ERROR).jsend.error(err.message);
  }
});

/*
Running main engine
*/
// app.listen(config.port, config.host, () => {
//   logger.info('========================\n');
//   logger.info(`Port: ${config.port}`);
//   logger.info(`Host: ${config.host}`);
//   logger.info(`Debug: ${config.debug}`);
//   logger.info(`Log Level: ${config.logLevel}\n`);
//   logger.info('========================\n');
// });

/*
Closing mongo connection when nodejs exitting process
*/
process.on('SIGINT', () => {
  mongo.mongoose.connection.close(() => {
    logger.info('Closing mongo connection...');
    process.exit(0);
  });
});

module.exports = app;
