/* eslint-disable no-underscore-dangle */
const _convertDebug = env => ((env === 'On'));

const config = {
  port: 8000,
  host: '0.0.0.0',
  debug: _convertDebug(process.env.DEBUG),
  logLevel: process.env.LOG_LEVEL || 'debug',
  mongoUri: process.env.MONGO_URI || 'mongodb://localhost:27017/bambu_dev',

  pagination: {
    limit: 10,
  },
};

module.exports = config;
