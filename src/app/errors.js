/* eslint-disable max-len */
const http = require('http-status-codes');

class AppError extends Error {
  constructor(message, status) {
    super(message);

    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);

    this.status = status || http.INTERNAL_SERVER_ERROR;
  }
}

class DuplicateObjectError extends AppError {
  constructor(message) {
    super(message, http.CONFLICT);
  }
}

class ResourceNotFound extends AppError {
  constructor() {
    super('Your requested resource not found.', http.NOT_FOUND);
  }
}

class TokenExpired extends AppError {
  constructor() {
    super('Your token has expired.', http.NOT_FOUND);
  }
}

class InvalidAuthCredentials extends AppError {
  constructor() {
    super('The email or password that you have entered is incorrect.', http.UNAUTHORIZED);
  }
}

class ValidationError extends AppError {
  constructor(message) {
    super(message, http.BAD_REQUEST);
  }
}

module.exports = {
  AppError, DuplicateObjectError, ResourceNotFound, TokenExpired, InvalidAuthCredentials, ValidationError,
};
