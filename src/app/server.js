const app = require('./app');
const config = require('./config');
const utils = require('./utils');

const { logger } = utils;

app.listen(config.port, config.host, () => {
  logger.info('======= bambudev =========\n');
  logger.info(`Port: ${config.port}`);
  logger.info(`Host: ${config.host}`);
  logger.info(`Debug: ${config.debug}`);
  logger.info(`Log Level: ${config.logLevel}\n`);
  logger.info('========================\n');
});

