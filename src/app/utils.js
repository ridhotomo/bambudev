/*
I'm suggest using Winston as our logger
Ref: https://github.com/winstonjs/winston/tree/2.x
*/
const winston = require('winston');

const config = require('./config');


/*
This part is important, we need to configure
logging level and also logging transports.
We can use multiple transport at once.
*/
const logger = new winston.Logger({
  level: config.logLevel,
  transports: [
    new (winston.transports.Console)(),
    // new winston.transports.File({ filename: 'error.log', level: 'error' }),
    // new winston.transports.File({ filename: 'combined.log' }),
  ],
});

module.exports = { logger };
