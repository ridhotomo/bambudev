/* eslint-disable no-console, no-underscore-dangle, consistent-return */
const express = require('express');
const jsend = require('jsend');
const http = require('http-status-codes');
const validate = require('validate.js');

const model = require('./models');

const createService = require('./services/create');
const listsService = require('./services/lists');

const router = express.Router();

// use jsend
router.use(jsend.middleware);


router.post('/', async (req, res, next) => {
  try {
    const validation = validate(req.body, model.PeopleValidationRules);
    if (validation) {
      return res.status(http.BAD_REQUEST).jsend.fail(validation);
    }

    const doc = await createService.create(model, req.body);
    delete doc.__v;
    delete doc.password;

    res.status(http.CREATED).jsend.success(doc);
  } catch (err) {
    next(err);
  }
});

router.get('/', async (req, res, next) => {
  try {
    const query = req.query ? req.query : {};
    const doc = await listsService.list(model, query);
    res.status(http.OK).jsend.success({ peopleLikeYou: doc });
  } catch (err) {
    next(err);
  }
});

module.exports = router;
