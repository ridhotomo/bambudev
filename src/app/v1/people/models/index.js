const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');

const { Schema } = mongoose;

const peopleSchema = new Schema({
  name: { type: String, index: true, trim: true },
  age: { type: Number, index: true },
  latitude: { type: Number, trim: true },
  longitude: { type: Number, index: true },
  monthlyIncome: { type: Number, index: true },
  experienced: { type: Boolean, default: false },
  score: { type: Number, index: true },
});

const PeopleValidationRules = {
  name: {
    presence: true,
  },
  age: {
    presence: true,
  },
  score: {
    presence: true,
  },
};
peopleSchema.index({ firstName: 'text', lastName: 'text' });
peopleSchema.plugin(paginate);

const People = mongoose.model('People', peopleSchema);
People.ensureIndexes();

module.exports = { People, PeopleValidationRules };
