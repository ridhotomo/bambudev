/* eslint-disable no-console, no-underscore-dangle, consistent-return */

const create = async (model, payload) => {
  const doc = new model.People(payload);
  const created = await doc.save();

  return created.toJSON();
};

module.exports = { create };
