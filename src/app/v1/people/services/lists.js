/* eslint-disable no-console, no-underscore-dangle, consistent-return */
const list = async (model, queryParam) => {
  const query = queryParam;
  const score = { score: -1 };
  const users = await model.People.find(query).sort(score).select('-__v');

  return users;
};

module.exports = {
  list,
};
