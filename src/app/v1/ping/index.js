/* eslint-disable no-console  */
const express = require('express');
const jsend = require('jsend');
const http = require('http-status-codes');
const utils = require('../../utils');

const router = express.Router();

const { logger } = utils;

// use jsend
router.use(jsend.middleware);

router.get('/', async (req, res) => {
  const testAsync = () => Promise.resolve('test async');

  const testAwait = await testAsync();
  logger.debug(testAwait);
  res.status(http.OK).jsend.success('pong');
});

module.exports = router;
